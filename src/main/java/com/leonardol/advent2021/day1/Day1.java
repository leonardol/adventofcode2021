package com.leonardol.advent2021.day1;

import com.leonardol.advent2021.util.Advent2021Util;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class Day1 {

    public int calculateCounter(InputStream is) {
        List<Integer> valueList = Advent2021Util.getValueList(is);
        return calculateCounter(valueList);
    }
    public int calculateCounter(List<Integer> sonarDepths) {

        AtomicInteger counter = new AtomicInteger(0);
        if (sonarDepths != null && sonarDepths.size() > 1) {
            AtomicInteger aux = new AtomicInteger(sonarDepths.get(0));
            sonarDepths.stream().forEach(sonarDepth -> {
                if (sonarDepth > aux.get()) counter.getAndIncrement();
                aux.getAndSet(sonarDepth);
            });
        }
        return counter.get();
    }

    public int calculateWindowCounter(InputStream is) {
        List<Integer> valueList = Advent2021Util.getValueList(is);
        return calculateWindowCounter(valueList);
    }

    public int calculateWindowCounter(List<Integer> sonarDepths) {
        List<Integer> depthWindow = new ArrayList<>();
        if (sonarDepths != null && sonarDepths.size() > 1) {
            int[] aux = new int[]{sonarDepths.get(0) + sonarDepths.get(1), sonarDepths.get(1), 0};
            int bound = sonarDepths.size();
            for (int i = 2; i < bound; i++) {
                aux[0] += sonarDepths.get(i);
                aux[1] += sonarDepths.get(i);
                aux[2] += sonarDepths.get(i);
                if ((i + 1) % 3 == 0) {
                    depthWindow.add(aux[0]);
                    aux[0] = 0;
                } else if (i % 3 == 0) {
                    depthWindow.add(aux[1]);
                    aux[1] = 0;
                } else if ((i - 1) % 3 == 0) {
                    depthWindow.add(aux[2]);
                    aux[2] = 0;
                }
            }
        }
        return calculateCounter(depthWindow);
    }
}
