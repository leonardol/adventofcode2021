package com.leonardol.advent2021.day1;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Day1Test {

    @Test
    public void canProcessInputStream() {
        Day1 day1 = new Day1();
        String data = "199" + "\n200" + "\n208" + "\n210" + "\n200"+ "\n207" + "\n240" + "\n269" + "\n260" + "\n263";
        InputStream is = (InputStream) new ByteArrayInputStream(data.getBytes());

        int counter = day1.calculateCounter(is);

        assertThat(counter).isEqualTo(7);
    }

    @Test
    public void canProcessMeasurementList() {
        Day1 day1 = new Day1();
        List<Integer> testMeasurementList = Arrays.asList(199, 200, 208, 210, 200, 207, 240, 269, 260, 263);

        assertThat(day1.calculateCounter(testMeasurementList)).isEqualTo(7);
    }

    @Test
    public void canProcessMeasurementFile() {
        Day1 day1 = new Day1();
        String fileName = "day_1_input.txt";
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream(fileName);

        int counter = day1.calculateCounter(is);

        assertThat(counter).isEqualTo(1288);
    }

    @Test
    public void canProcessWindowList() {
        Day1 day1 = new Day1();
        List<Integer> testMeasurementList = Arrays.asList(199, 200, 208, 210, 200, 207, 240, 269, 260, 263);

        assertThat(day1.calculateWindowCounter(testMeasurementList)).isEqualTo(5);
    }

    @Test
    public void canProcessWindowFile() {
        Day1 day1 = new Day1();
        String fileName = "day_1_input.txt";
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream(fileName);

        int counter = day1.calculateWindowCounter(is);

        assertThat(counter).isEqualTo(1311);
    }
}