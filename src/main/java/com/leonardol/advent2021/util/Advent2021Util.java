package com.leonardol.advent2021.util;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
public class Advent2021Util {
    public static List<Integer> getValueList(InputStream is) {
        try {
            return extractValueList(is);
        } catch(IOException e) {
            log.error("Resource retrieval error", e);
        } finally {
            closeInputStream(is);
        }
        return Collections.emptyList();
    }

    private static List<Integer> extractValueList(InputStream is) throws IOException {
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        List<Integer> values = new ArrayList<>();
        String str;
        while ((str = br.readLine()) != null) {
            values.add(Integer.parseInt(str));
        }
        return values;
    }

    private static void closeInputStream(InputStream is) {
        if (Optional.ofNullable(is).isPresent()) {
            try {
                is.close();
            } catch (IOException e) {
                log.error("Resource retrieval error", e);
            }
        }
    }
}
